package comp2500;

public class Config {

	/**
	 * Config This class contains the default constants for the program
	 */

	protected static final String FILENAME = "Addressbook.ser";
	// path to store persistent data
	protected static final String DEFAULTPROTRAIT = "resources/contact.jpg";
	protected static final String ABOUTPIC = "resources/about.jpg";
	protected static final String DONATEPIC = "resources/instantnoodle.jpg";
	protected static final int PICSIZE = 150;
	protected static final String LOADCOMMAND = "LOAD";
	protected static final String SAVECOMMAND = "SAVE";
	protected static final String EXITCOMMAND = "EXIT";
	public static final String SORTCOMMAND = "SORT";
	protected static final String QUERYCOMMAND = "QUERY";
	protected static final String ABOUTCOMMAND = "ABOUT";

}
