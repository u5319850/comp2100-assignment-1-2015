package comp2500;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class POTest {

	/**
	 * POTest This is the test for the program, ensuring the stability and
	 * robustness of the program. Running the test does not affect current data.
	 * 
	 * @author Hao
	 */

	PersonalOrganiser PO = new PersonalOrganiser();
	int l = PO.gui.contacts.size();

	@Test
	// Test for adding a contact
	public void testAdd() {
		add("Test");
		assertEquals(PO.gui.contacts.size(), l + 1);
	}

	@Test
	// Test for editing a contact
	public void testEdit() {
		add("Unchanged");
		editName(l, "Changed");
		assertEquals(PO.gui.contacts.get(l).name, "Changed");
	}

	@Test
	// Test for deleting a contact
	public void testDelete() {
		add("Test");
		delete(l);
		assertEquals(PO.gui.contacts.size(), l);
	}

	@Test
	// Test for reset
	public void testReset() {
		add("Test1");
		add("Test2");
		reset();
		assertEquals(PO.gui.contacts.size(), 0);
	}

	@Test
	// Test for up/down
	public void testMove() {
		add("first");
		add("second");
		Contact first = PO.gui.contacts.get(l);
		PO.gui.jlist.setSelectedIndex(l);
		PO.gui.down.doClick();
		assertEquals(PO.gui.contacts.get(l + 1), first);
		PO.gui.up.doClick();
		assertEquals(PO.gui.contacts.get(l), first);
	}

	public void add(String name) {
		PO.gui.jtfname.setText(name);
		PO.gui.badd.doClick();
	}

	public void editName(int i, String name) {
		PO.gui.jlist.setSelectedIndex(i);
		PO.gui.jtfname.setText(name);
		PO.gui.bsave.doClick();
	}

	public void delete(int i) {
		PO.gui.jlist.setSelectedIndex(i);
		PO.gui.bdel.doClick();
	}

	private void reset() {
		PO.gui.breset.doClick();
	}
}
