package comp2500;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.DefaultListModel;

public class IOPersistence {

	/**
	 * IOPersistance This class deals with storing and reading of contacts
	 * information
	 * 
	 * @param filename
	 *            File that stores the persistent data, default settiing in
	 *            Config
	 * @return None
	 */

	@SuppressWarnings("unchecked")
	public static DefaultListModel<Contact> load(String filename) {
		DefaultListModel<Contact> addressbook = new DefaultListModel<Contact>();
		try {
			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
					filename));
			addressbook = (DefaultListModel<Contact>) ois.readObject();
		} catch (IOException e) {
			addressbook = new DefaultListModel<Contact>();
		} catch (ClassNotFoundException e) {
			addressbook = new DefaultListModel<Contact>();
		}
		return addressbook;
	}

	public static void save(String filename, DefaultListModel<Contact> contacts) {
		try {
			// recreate the file to avoid dirty data
			File f = new File(filename);
			f.delete();
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(filename));
			oos.reset();
			oos.writeObject(contacts);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
