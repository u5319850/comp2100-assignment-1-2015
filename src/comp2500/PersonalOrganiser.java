package comp2500;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PersonalOrganiser extends JFrame implements ActionListener,
		ListSelectionListener {

	/**
	 * PersonalOrganiser This is a personal organiser program 
	 * This class deal with all the listeners to the elements
	 * 
	 * @author Hao <u5319850>
	 */
	private static final long serialVersionUID = 1L;

	GUI gui;
	Update update;

	public static void main(String[] args) {
		new PersonalOrganiser();
	}

	// Update the JList and display whenever its elements are changed
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() == gui.jlist) {
			if (!gui.jlist.isSelectionEmpty()) {
				Update.displayContact(gui, gui.jlist.getSelectedValue());
				gui.bar.updateUI();
			}
		}
	}

	// Call the corresponding methods to a specific event
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == gui.badd) {
			String name = gui.jtfname.getText();
			if (!name.isEmpty()) {
				// check for duplicated contacts
				for (int i = 0; i < gui.contacts.size(); i++) {
					if (gui.contacts.get(i).name.equals(name)) {
						JOptionPane.showMessageDialog(this,
								"Duplicated contacts!");
						return;
					}
				}
				addContact();
			} else {
				JOptionPane.showMessageDialog(this, "Name missing");
			}
			gui.jlist.clearSelection();
			// once contacts are changed, update menu bar to avoid overlapping
			gui.bar.updateUI();
		}

		if (e.getSource() == gui.bsave) {
			// only update contact when there is one selected
			if (gui.jlist.getSelectedValue() != null) {
				Update.updateContact(gui, gui.jlist.getSelectedValue());
				gui.bar.updateUI();
			} else {
				JOptionPane.showMessageDialog(this, "No contact selected");
			}
		}

		if (e.getSource() == gui.bclear) {
			Update.resetTextField(gui);
			if (!gui.jlist.isSelectionEmpty()) {
				gui.jlist.clearSelection();
			}
		}

		if (e.getSource() == gui.up) {
			int n = gui.jlist.getSelectedIndex();
			// -1 for no selection, 0 for already on top
			if (n > 0) {
				Contact t = gui.contacts.get(n);
				gui.contacts.set(n, gui.contacts.get(n - 1));
				gui.contacts.set(n - 1, t);
				gui.jlist.setSelectedIndex(n - 1);
				gui.bar.updateUI();
			}
		}

		if (e.getSource() == gui.down) {
			int n = gui.jlist.getSelectedIndex();
			if (n != -1 && n != gui.contacts.size() - 1) {
				Contact t = gui.contacts.get(n);
				gui.contacts.set(n, gui.contacts.get(n + 1));
				gui.contacts.set(n + 1, t);
				gui.jlist.setSelectedIndex(n + 1);
				gui.bar.updateUI();
			}
		}

		if (e.getSource() == gui.bdel) {
			int n = gui.jlist.getSelectedIndex();
			if (n != -1) {
				gui.contacts.remove(n);
				gui.bar.updateUI();
			}
		}

		if (e.getSource() == gui.breset) {
			gui.contacts.clear();
			gui.bar.updateUI();
		}

		if (e.getActionCommand() == Config.LOADCOMMAND) {
			JFileChooser filechooser = new JFileChooser();
			int res = filechooser.showOpenDialog(gui.f);
			if (res == JFileChooser.APPROVE_OPTION) {
				String fileurl = filechooser.getSelectedFile()
						.getAbsolutePath();
				DefaultListModel<Contact> contacts = IOPersistence
						.load(fileurl);
				gui.contacts.clear();
				for (int i = 0; i < contacts.size(); i++) {
					gui.contacts.addElement(contacts.get(i));
				}
			}
			gui.bar.updateUI();
		}

		if (e.getActionCommand() == Config.SAVECOMMAND) {
			IOPersistence.save(Config.FILENAME, gui.contacts);
		}

		if (e.getActionCommand() == Config.EXITCOMMAND) {
			IOPersistence.save(Config.FILENAME, gui.contacts);
			System.exit(0);
		}

		if (e.getActionCommand() == Config.ABOUTCOMMAND) {
			JOptionPane.showMessageDialog(this, GUI.about());
		}

		if (e.getActionCommand() == Config.SORTCOMMAND) {
			Update.sortContact(gui.contacts);
			gui.bar.updateUI();
		}

		if (e.getActionCommand() == Config.QUERYCOMMAND) {
			String query = JOptionPane.showInputDialog("Name: ");
			if (query == null) {
				JOptionPane.showMessageDialog(this, "No information entered!");
			} else {
				queryContact(query);
			}
		}
		
		if (e.getActionCommand() == "NUMBER") {
			String query = JOptionPane.showInputDialog("Name: ");
			if (query != null) {
				JOptionPane.showMessageDialog(this, "Number of contacts: "+update.nofcontacts(gui.contacts, query));
			gui.bar.updateUI();
			}
		}

		if (e.getActionCommand() == "DONATE") {
			JOptionPane.showMessageDialog(this, GUI.donate());
			gui.bar.updateUI();
		}
	}

	/**
	 * addContact This method add a contact to the JList and reset the
	 * textfields.
	 * 
	 * @param None
	 * @return None
	 */

	private void addContact() {
		Contact contact = new Contact(gui.potrait.getImage(),
				gui.jtfname.getText(), gui.csex.getSelectedItem(),
				gui.cage.getSelectedIndex(), gui.jtfemail.getText(),
				gui.cdepart.getSelectedItem(), gui.jtftel.getText(),
				gui.jtfaddr.getText());
		Update.updateContact(gui, contact);
		gui.contacts.addElement(contact);
		Update.resetTextField(gui);
	}

	/**
	 * queryContact This method search for a specific contact name within
	 * existing contacts.
	 * 
	 * @param query
	 *            Name of the contact.
	 * @return None
	 */

	private void queryContact(String query) {
		// an array for indices of potential multiple contact objects
		int n = gui.contacts.size();
		int[] tar = new int[n];
		int j = 0;
		for (int i = 0; i < gui.contacts.size(); i++) {
			if (gui.contacts.get(i).name.equals(query)) {
				tar[j] = i;
				j++;
			}
		}
		// make the rest of the array out of index, otherwise the first
		// contact(index 0) will be selected wrongly
		for (int i = j; i < gui.contacts.size(); i++) {
			tar[i] = gui.contacts.size();
		}
		if (tar != null) {
			gui.jlist.setSelectedIndices(tar);
		}
	}

	// Add listensers to elements
	public PersonalOrganiser() {
		gui = new GUI();
		update = new Update();
		gui.menuiload.addActionListener(this);
		gui.menuisave.addActionListener(this);
		gui.menuiexit.addActionListener(this);
		gui.menuisort.addActionListener(this);
		gui.menuiquery.addActionListener(this);
		gui.menuinum.addActionListener(this);
		gui.menuidonate.addActionListener(this);
		gui.menuiabout.addActionListener(this);
		gui.up.addActionListener(this);
		gui.down.addActionListener(this);
		gui.bdel.addActionListener(this);
		gui.breset.addActionListener(this);
		gui.badd.addActionListener(this);
		gui.bsave.addActionListener(this);
		gui.bclear.addActionListener(this);
		gui.jlist.addListSelectionListener(this);
	}

}