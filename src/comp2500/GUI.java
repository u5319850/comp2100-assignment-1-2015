package comp2500;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class GUI extends JFrame {
	/**
	 * GUI This class implements the GUI interface of the program Using JSwing
	 * 
	 * @author Hao
	 */
	private static final long serialVersionUID = 1L;

	JFrame f;
	// initialize contacts list from file storage
	DefaultListModel<Contact> contacts = IOPersistence.load(Config.FILENAME);
	JList<Contact> jlist = new JList<Contact>(contacts);
	JLabel pic, name, sex, age, email, depart, tel, addr;
	JTextField jtfname, jtfemail, jtftel, jtfaddr;
	Choice csex, cage, cdepart;
	JMenuBar bar;
	JButton bsave, badd, up, down, bdel, b3, breset, bclear, selectfilebutton;
	ImageIcon potrait;
	JMenu menufile, menuedit, menuhelp;
	JMenuItem menuiload, menuisave, menuisort, menuiquery, menuinum, menuiexit, menuiabout,
			menuidonate;
	Panel lPanel;

	/**
	 * GUI This constructs the main UI of the program Adds all Swing elements to
	 * Panels then to the main frame
	 */
	public GUI() {
		// Create a frame
		f = new JFrame("Personal Organiser");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Set the size of panel
		f.setSize(600, 500);
		// Set initial location
		f.setLocation(350, 150);

		// Set the menu
		bar = new JMenuBar();

		menufile = new JMenu("File");
		menuedit = new JMenu("Edit");
		menuhelp = new JMenu("Help");

		menuiload = new JMenuItem("Load");
		menuiload.setActionCommand(Config.LOADCOMMAND);
		menufile.add(menuiload);
		
		menuisave = new JMenuItem("Save");
		menuisave.setActionCommand(Config.SAVECOMMAND);
		menufile.add(menuisave);

		menuiexit = new JMenuItem("Exit");
		menuiexit.setActionCommand(Config.EXITCOMMAND);
		menufile.add(menuiexit);

		menuisort = new JMenuItem("Sort");
		menuisort.setActionCommand(Config.SORTCOMMAND);
		menuedit.add(menuisort);

		menuiquery = new JMenuItem("Query");
		menuiquery.setActionCommand(Config.QUERYCOMMAND);
		menuedit.add(menuiquery);
		
		menuinum = new JMenuItem("Prefix");
		menuinum.setActionCommand("NUMBER");
		menuedit.add(menuinum);

		menuidonate = new JMenuItem("Donate");
		menuidonate.setActionCommand("DONATE");
		menuhelp.add(menuidonate);

		menuiabout = new JMenuItem("About");
		menuiabout.setActionCommand(Config.ABOUTCOMMAND);
		menuhelp.add(menuiabout);

		bar.add(menufile);
		bar.add(menuedit);
		bar.add(menuhelp);

		// function buttons
		up = new JButton("UP");

		down = new JButton("DOWN");

		bdel = new JButton("DELETE");
		breset = new JButton("RESET");

		Panel buttonPanel = new Panel(new GridLayout(2, 2));
		buttonPanel.add(up);
		buttonPanel.add(down);
		buttonPanel.add(bdel);
		buttonPanel.add(breset);

		// Left-hand-side panel

		lPanel = new Panel(new BorderLayout());

		lPanel.add(new JScrollPane(jlist));
		lPanel.add(buttonPanel, BorderLayout.SOUTH);

		// Personal information fields
		Panel protraitpic = new Panel(new BorderLayout());
		potrait = new ImageIcon(new ImageIcon(Config.DEFAULTPROTRAIT)
				.getImage().getScaledInstance(Config.PICSIZE, Config.PICSIZE,
						Image.SCALE_DEFAULT));
		pic = new JLabel(potrait);
		protraitpic.add(pic, BorderLayout.CENTER);
		JFileChooser filechooser = new JFileChooser();
		selectfilebutton = new JButton("Change");
		selectfilebutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int res = filechooser.showOpenDialog(f);
				if (res == JFileChooser.APPROVE_OPTION) {
					String picurl = filechooser.getSelectedFile()
							.getAbsolutePath();
					potrait = new ImageIcon(new ImageIcon(picurl).getImage()
							.getScaledInstance(Config.PICSIZE, Config.PICSIZE,
									Image.SCALE_DEFAULT));
					pic.setIcon(potrait);
				}
			}
		});
		protraitpic.add(selectfilebutton, BorderLayout.SOUTH);

		name = new JLabel("name");
		jtfname = new JTextField("", 20);
		tel = new JLabel("tel");
		jtftel = new JTextField("", 20);
		sex = new JLabel("gender");
		csex = new Choice();
		csex.add("Male");
		csex.add("Female");
		age = new JLabel("age");
		cage = new Choice();
		for (int i = 0; i < 100; i++)
			cage.addItem(String.valueOf(i));
		email = new JLabel("email");
		jtfemail = new JTextField("", 20);
		Panel personalinfoPanel = new Panel(new GridLayout(6, 2));
		personalinfoPanel.add(name);
		personalinfoPanel.add(jtfname);
		personalinfoPanel.add(sex);
		personalinfoPanel.add(csex);
		personalinfoPanel.add(age);
		personalinfoPanel.add(cage);
		personalinfoPanel.add(tel);
		personalinfoPanel.add(jtftel);
		personalinfoPanel.add(email);
		personalinfoPanel.add(jtfemail);

		Panel addinfoPanel = new Panel(new GridLayout(2, 2));
		addr = new JLabel("address");
		jtfaddr = new JTextField("");
		depart = new JLabel("relation");
		cdepart = new Choice();
		cdepart.add("Friend");
		cdepart.add("Colleague");
		cdepart.add("Family");
		cdepart.add("Other");
		addinfoPanel.add(depart);
		addinfoPanel.add(cdepart);
		addinfoPanel.add(addr);
		addinfoPanel.add(jtfaddr);

		Panel acbuttonPanel = new Panel(new GridLayout(1, 2));
		badd = new JButton("Add");

		bsave = new JButton("Save");

		bclear = new JButton("Clear");

		acbuttonPanel.add(badd);
		acbuttonPanel.add(bsave);
		Panel modibuttonPanel = new Panel(new GridLayout(2, 1));
		modibuttonPanel.add(acbuttonPanel);
		modibuttonPanel.add(bclear);

		Panel brPanel = new Panel(new BorderLayout());
		brPanel.add(addinfoPanel);
		brPanel.add(modibuttonPanel, BorderLayout.SOUTH);

		// Right-hand-side Panel
		Panel rPanel = new Panel(new GridLayout(3, 1));
		rPanel.add(protraitpic);
		rPanel.add(personalinfoPanel);
		rPanel.add(brPanel);

		Panel mainPanel = new Panel(new GridLayout(1, 2));
		mainPanel.add(lPanel);
		mainPanel.add(rPanel);

		f.getContentPane().add(mainPanel);
		f.setJMenuBar(bar);
		f.setVisible(true);
	}

	/**
	 * about This implements the about item in About menu
	 */
	static Panel about() {
		Panel about = new Panel(new BorderLayout());
		JLabel title = new JLabel("Personal Organiser Program");
		about.add(title, BorderLayout.NORTH);
		ImageIcon ime = new ImageIcon(new ImageIcon(Config.ABOUTPIC).getImage()
				.getScaledInstance(200, 200, Image.SCALE_DEFAULT));
		JLabel me = new JLabel(ime);
		about.add(me, BorderLayout.CENTER);
		JLabel author = new JLabel("Author: Hao Zhang");
		JLabel uid = new JLabel("UID: u5319850");
		Panel info = new Panel(new GridLayout(2, 1));
		info.add(author);
		info.add(uid);
		about.add(info, BorderLayout.SOUTH);
		return about;
	}

	/**
	 * donate This implements the donate item in About menu
	 */
	static Panel donate() {
		Panel donate = new Panel(new BorderLayout());
		ImageIcon inoodle = new ImageIcon(new ImageIcon(Config.DONATEPIC)
				.getImage().getScaledInstance(Config.PICSIZE, Config.PICSIZE,
						Image.SCALE_DEFAULT));
		JLabel noodle = new JLabel(inoodle);
		JLabel plz = new JLabel(
				"Please donate some $ to me for a cup of instant noodle");
		donate.add(plz, BorderLayout.NORTH);
		donate.add(noodle, BorderLayout.CENTER);
		JLabel name = new JLabel("Name: Hao Zhang");
		JLabel bsb = new JLabel("BSB: 062903");
		JLabel an = new JLabel("Account No: 1073 4518");
		Panel info = new Panel(new GridLayout(3, 1));
		info.add(name);
		info.add(bsb);
		info.add(an);
		donate.add(info, BorderLayout.SOUTH);
		return donate;
	}
}
