package comp2500;

import java.awt.Image;
import java.util.Arrays;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;

public class Update {

	/**
	 * Update This class implements all the response to user behavior
	 * 
	 * @author Hao
	 */

	/**
	 * displayContact This method display information of the selected contact
	 * 
	 * @param gui
	 *            Main frame of the program interface
	 * @param contact
	 *            The selected contact information
	 */
	static void displayContact(GUI gui, Contact contact) {
		gui.potrait = contact.potrait;
		gui.pic.setIcon(contact.potrait);
		gui.jtfname.setText(contact.name);
		gui.csex.select(contact.gender + "");
		gui.cage.select(contact.age + "");
		gui.jtfemail.setText(contact.email);
		gui.cdepart.select(contact.group);
		gui.jtftel.setText(contact.tel);
		gui.jtfaddr.setText(contact.address);
	}

	/**
	 * updateContact This method update the contact information when user
	 * modifies
	 * 
	 * @param gui
	 * @param contact
	 */
	static void updateContact(GUI gui, Contact contact) {
		contact.potrait = gui.potrait;
		contact.name = gui.jtfname.getText();
		contact.gender = gui.csex.getSelectedItem();
		contact.age = gui.cage.getSelectedIndex();
		contact.email = gui.jtfemail.getText();
		contact.group = gui.cdepart.getSelectedItem();
		contact.tel = gui.jtftel.getText();
		contact.address = gui.jtfaddr.getText();
	}

	/**
	 * sortContact This method sorts the contacts alphabetically, by convert
	 * DeafultListModel to array, sort and then copy back.
	 * 
	 * @param contacts
	 *            DeafultListModel that contains all contacts
	 */
	static void sortContact(DefaultListModel<Contact> contacts) {
		Object[] objArray = (Object[]) contacts.toArray();
		Contact[] contactsArray = new Contact[objArray.length];
		// Iterate through the object array to convert it to Contact array
		for (int i = 0; i < objArray.length; i++) {
			contactsArray[i] = (Contact) objArray[i];
		}
		Arrays.sort(contactsArray);
		contacts.clear();
		for (int i = 0; i < contactsArray.length; i++) {
			contacts.addElement(contactsArray[i]);
		}
	}

	/**
	 * resetProtrait This method reset the contact portrait to the default
	 * 
	 * @param gui
	 */
	static void resetPotrait(GUI gui) {
		gui.potrait = new ImageIcon(new ImageIcon(Config.DEFAULTPROTRAIT)
				.getImage().getScaledInstance(Config.PICSIZE, Config.PICSIZE,
						Image.SCALE_DEFAULT));
		gui.pic.setIcon(gui.potrait);
	}

	/**
	 * resetTextField This method clear the text field on user interface
	 * 
	 * @param gui
	 */
	static void resetTextField(GUI gui) {
		resetPotrait(gui);
		gui.jtfname.setText("");
		gui.csex.select(0);
		gui.cage.select(0);
		gui.jtfemail.setText("");
		gui.cdepart.select(0);
		gui.jtftel.setText("");
		gui.jtfaddr.setText("");
	}

	public String nofcontacts(DefaultListModel<Contact> contacts, String name) {
		int n = 0;
		for (int i = 0; i < contacts.getSize(); i++) {
			Contact contact = contacts.elementAt(i);
			if (contact.name.startsWith(name)) {
				n++;
			}
		}
		return n+"";
	}

}
