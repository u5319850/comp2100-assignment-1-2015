package comp2500;

import java.awt.Image;
import java.io.Serializable;

import javax.swing.ImageIcon;

public class Contact implements Serializable, Comparable {

	/**
	 * Contact This class is a template for a contact
	 * 
	 * @author Hao
	 */
	private static final long serialVersionUID = 1L;

	ImageIcon potrait;
	String name;
	String gender;
	int age;
	String email;
	String group;
	String tel;
	String address;

	public Contact() {

	}

	public Contact(Image pic, String name, String gender, int age,
			String email, String group, String tel, String address) {
		this.potrait = new ImageIcon(pic.getScaledInstance(Config.PICSIZE,
				Config.PICSIZE, Image.SCALE_DEFAULT));
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.email = email;
		this.group = group;
		this.tel = tel;
		this.address = address;
	}

	public String toString() {
		// Only name is shown on contacts list by default, detail information
		// will be display at right
		return name;
		// return name+"  "+gender+"  "+email+"  "+group+"  "+tel+"  "+address;
	}

	@Override
	public int compareTo(Object o) {
		Contact ocontact = (Contact) o;
		return this.name.compareTo(ocontact.name);
	}

}
